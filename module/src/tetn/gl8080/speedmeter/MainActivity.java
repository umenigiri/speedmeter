package tetn.gl8080.speedmeter;

import java.text.DecimalFormat;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {
    
    private LocationManager locationManager;
    private TextView speed;
    private TextView location;
    private DecimalFormat decimalFormat = new DecimalFormat("0.0");
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        this.locationManager = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        this.speed = (TextView)this.findViewById(R.id.speed);
        this.location = (TextView)this.findViewById(R.id.location);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        
        if (this.locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            this.locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, this.listener);
        } else {
            this.showSettings();
        }
    }
    
    private void showSettings() {
        new AlertDialog.Builder(this)
            .setMessage("GPSが有効になっていません。\n有効化しますか？")
            .setCancelable(false)
            .setPositiveButton("GPS設定起動",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id){
                        Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(callGPSSettingIntent);
                    }
                })
            .setNegativeButton("キャンセル",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                })
            .create()
            .show();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        this.locationManager.removeUpdates(this.listener);
    }
    
    private LocationListener listener = new LocationListener() {
        
        @Override
        public void onLocationChanged(Location loc) {
            String msg = String.format("speed=%f (%f, %f)", loc.getSpeed(), loc.getLatitude(), loc.getLongitude());
            Log.v("speedmeter", "onLocationChanged. " + msg);
            
            String label = decimalFormat.format(loc.getSpeed());
            speed.setText(label + " m/s");
            
            location.setText(loc.getLatitude() + ", " + loc.getLongitude());
        }
        
        @Override public void onStatusChanged(String provider, int status, Bundle extras) {}
        @Override public void onProviderEnabled(String provider) {}
        @Override public void onProviderDisabled(String provider) {}
    };
}
